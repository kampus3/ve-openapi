// Node modules
const fetch = require('node-fetch'); //=> https://www.npmjs.com/package/node-fetch


/**
 * Send AJAX request with Fetch API
 * @param {string} method - HTTP request method.
 * @param {string} path - HTTP request path.
 * @param {string} origin - Request sender origin URI.
 * @param {string} token - Request identity token.
 * @param {object} identity - Extracted identity informations.
 * @param {object} body - Requested body object.
 * @param {object} headers - Pre-defined request headers.
 */
const fetchCrudRequest = (method, path, origin, token, identity, body, headers = null) => {
    return new Promise( (resolve, reject) => {
        // Set HTTP method and headers
        let fetchOption = { method: method }
        if(headers){ fetchOption.headers = headers }
        else{
            fetchOption.headers = {
                'Origin': origin,
                'Content-Type': 'application/json',
            }
        }

        // Define fetch authentication and body
        if(token !== null){ fetchOption.headers['Authorization'] = `Bearer ${ token }` };
        if(identity !== null){ fetchOption.headers['x-request-id'] = `${ identity }` };
        if(body !== null){ fetchOption.body = JSON.stringify(body) };

        // Send fetch request
        fetch( path, fetchOption)
        .then( apiResponse => {
            if(apiResponse.ok){ return apiResponse.json(apiResponse) }
            else{ return reject(apiResponse) }
        })
        .then( jsonResposne  => resolve(jsonResposne))
        .catch( objectApiResponseError => reject(objectApiResponseError))
    })
}

// Export module functions
module.exports = {
    fetchCrudRequest,
}