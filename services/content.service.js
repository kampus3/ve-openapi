// Imports
const CryptoJS = require("crypto-js"); //=> https://www.npmjs.com/package/crypto-js
const jwt = require('jsonwebtoken'); //=> https://www.npmjs.com/package/jsonwebtoken
const { v4: uuidv4 } = require('uuid'); //=> https://www.npmjs.com/package/uuid"
const { fetchCrudRequest } = require("./fetch.service");

/**
 * Map case type to toggle camelCase/snake_case.
 * => https://javascript.plainenglish.io/convert-string-to-different-case-styles-snake-kebab-camel-and-pascal-case-in-javascript-da724b7220d7
 * @param {string} text - String to map.
 * @param {string} map - Initial case type ['camelCase' || 'snake_case'].
 */
const mapCase = (text, map = 'camelCase') => { 
    if( map === 'camelCase' ){
        return text.replace(/\.?([A-Z])/g, (x,y) => { return "_" + y.toLowerCase() }).replace(/^_/, "");
    }
    else if( map === 'snake_case' ){
        return text.replace( /[^a-zA-Z0-9]+(.)/g, (x,y) => y.toUpperCase() );;
    }
    else{ return text }
}

/**
 * Decrypt string with CryptoJS.
 * @param {string} object - Javascript object to decrypt.
 * @param {string} uncrypted - Uncrypted value list
 */
const decryptObjectValue = async( object, uncrypted ) => {
    if( object && uncrypted ){
        for( let prop of uncrypted){
            if(object[prop]){
                const decode = CryptoJS.AES.decrypt(object[prop], process.env.NODE_SHARED_CRYPTO_KEY);
                object[prop] = decode.toString(CryptoJS.enc.Utf8)
            } 
        }
    }

    // Return client body
    return object;
}

/**
 * Extract client request body to map value to PosqreSQL
 * @param {string} api - Active API name.
 * @param {string} version - API active version.
 * @param {string} originalUrl - Requested original URL.
 */
const mapRequestBody = ( body, description, model, verb, decoded ) =>{
    try {
        // Prepare validation values
        let ok = true;
        let miss = [];
        let extra = [];
        let idx = 2;

        // Used for MYSQL
        let content = {
            uuid: uuidv4()
        };
        
        // Used for PostgreSQL
        let sqlObject = {
            table: model,
            label: [ 'uuid' ],
            value: [ uuidv4() ],
            index: [ '$1' ],
        };

        if( Object.keys(body).length ){
            // Get each request client body properties
            for( let prop in body ){
                // Prepare returned value
                let requestObject = {}
    
                // Get mandatory/optional value
                if( description.mandatory ){ requestObject = Object.assign( requestObject, description.mandatory ) }
                if( description.optional ){ requestObject = Object.assign( requestObject, description.optional ) }
    
                // Define extra properties
                if( description.mandatory ){
                    if( Object.keys( requestObject ).indexOf(prop) === -1 ){ 
                        ok = false;
                        extra.push(prop)
                    }
                }
            }
    
            // Define returned object to prepare SQL inserts
            if(ok){
                for( let prop in body ){
                    if( description.crypted && description.crypted.indexOf(prop) !== -1 ){
                        content[mapCase(prop)] = CryptoJS.AES.encrypt(body[prop], process.env.NODE_SHARED_CRYPTO_KEY).toString();
                    }
                    else{ content[mapCase(prop)] = body[prop] }
    
                    // Define Postgres values
                    sqlObject.label.push(mapCase(prop))
                    sqlObject.value.push( content[mapCase(prop)] )
                    sqlObject.index.push(`$${idx}`)
                    idx++;
                }
    
                if( decoded && ['post'].indexOf(verb) !== -1 ){
                    sqlObject.label.push('author');
                    sqlObject.value.push(decoded.uuid);
                    sqlObject.index.push(`$${idx}`);
                    idx++;
                }
    
                // Return validation success
                return { ok, miss, extra, content, sqlObject }
            }
            else{ throw { ok, miss, extra, content: null, sqlObject: null } }
        }
        else{
            throw { ok: false, miss: description.mandatory, extra, content: null, sqlObject: null }
        }

    }
    catch (error) { 
        // Catch function error
        console.log(`[DEBUG] mapRequestBody() ERROR`, error )
        return error 
    }
}

/**
 * Extract and decode client JWT from request header
 * @param {string} token - Generated JWT access.
 */
const extractJwt = ( appName, sqlClient, token ) => { 
    return new Promise( async (resolve, reject) => {
        try {
            if( token ){
                // Check API name
                if( ['data'].indexOf( appName ) !== -1 ){
                    const identityResponse = await fetchCrudRequest( 
                        'get',
                        `${ process.env.NODE_IDENTITY_API_URL }/v1/auth/token`,
                        'http://localhost:8977',
                        token,
                    )

                    // Update request identity from 'identity.api'
                    return resolve( { isAuth: true, decoded: identityResponse.data, error: null } );
                }
                else if( ['identity'].indexOf( appName ) !== -1 ){
                    // Decode JWT
                    const decode = CryptoJS.AES.decrypt(token, process.env.NODE_SHARED_CRYPTO_KEY);
                    const decodeJwt = decode.toString(CryptoJS.enc.Utf8)

                    // Check JWT
                    jwt.verify( decodeJwt, process.env.NODE_SHARED_JWT_KEY, (err, decoded) => {
                        if(err !== null){ throw 'JWT not valide'}
                        else{ 
                            sqlClient.query(`SELECT id FROM identity WHERE email = '${ decoded.email }' AND uuid = '${ decoded.uuid }'`, function (error, results) {
                                if (error){ throw `Identity not found` }
                                else{
                                    return resolve({ isAuth: true, decoded, error: null }) 
                                }
                            })
                        }
                    });
                }
                else{ throw 'Unknow API name' }
            }
            else{ throw 'No token'}
        } 
        catch (error) {
            return resolve( { isAuth: false, decoded: null, error: error } )
        }
    })
}

// Export shared service functions
module.exports = {
    mapCase,
    decryptObjectValue,
    mapRequestBody,
    extractJwt
}