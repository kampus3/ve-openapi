// Imports
const mysql      = require('mysql');

/**
 * MYSQL database client
 * TODO: hide connection value inside '.env' API file
 */
const mysqlObject = mysql.createConnection({
  host     : 'localhost',
  port     : '8889',
  user     : 'root',
  password : 'root',
  database : 've-node'
});

// Export shared service objects
module.exports = {
    mysqlObject
}